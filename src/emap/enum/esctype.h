// Copyright (c) Copyright (c) Hercules Dev Team, licensed under GNU GPL.
// Copyright (c) 2014 - 2015 Evol developers

#ifndef EVOL_MAP_ENUM_ESCTYPE
#define EVOL_MAP_ENUM_ESCTYPE

typedef enum esc_type
{
    SC_PHYSICAL_SHIELD = 642,
} esc_type;

#endif  // EVOL_MAP_ENUM_ESCTYPE
